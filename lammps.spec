Name:           lammps
Version:        2023.08.02
Release:        3
Summary:        Molecular Dynamics Simulator
License:        GPLv2
Url:            https://lammps.sandia.gov
Source0:	    %{name}-%{version}-3.tar.gz
BuildRequires:  fftw-devel
BuildRequires:  gcc-c++
BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel
BuildRequires:  openmpi-devel
BuildRequires:  mpich-devel
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  fftw3-devel
BuildRequires:  zlib-devel
BuildRequires:  gsl-devel
BuildRequires:  voro++-devel
BuildRequires:  openblas-devel
BuildRequires:  hdf5-devel
BuildRequires:  kim-api-devel
BuildRequires:  cmake3 >= 3.1
BuildRequires:  ocl-icd-devel
BuildRequires:  opencl-headers
BuildRequires:  tbb-devel

%global lammps_desc \
LAMMPS is a classical molecular dynamics code, and an acronym for Large-scale \
Atomic/Molecular Massively Parallel Simulator.\
\
LAMMPS has potentials for soft materials (biomolecules, polymers) and \
solid-state materials (metals, semiconductors) and coarse-grained or \
mesoscopic systems. It can be used to model atoms or, more generically, as a \
parallel particle simulator at the atomic, meso, or continuum scale. \
\
LAMMPS runs on single processors or in parallel using message-passing \
techniques and a spatial-decomposition of the simulation domain. The code is \
designed to be easy to modify or extend with new functionality.

%description
%{lammps_desc}

%prep
%autosetup -n %{name}-%{version}-3

%build
mkdir build
cd build
cmake ../cmake -DCMAKE_INSTALL_PREFIX=$RPM_BUILD_ROOT/usr 
cmake --build . --parallel $RPM_BUILD_NCPUS

%install
cd build
make install
#install -d $RPM_BUILD_ROOT/usr/bin
#install -m644 lmp $RPM_BUILD_ROOT/usr/bin
#install -d $RPM_BUILD_ROOT/etc/profile.d
#install -d -m644 $RPM_BUILD_ROOT/etc/profile.d
#install -m644 etc/profile.d/lammps.sh $RPM_BUILD_ROOT/etc/profile.d
#install -m644 etc/profile.d/lammps.csh $RPM_BUILD_ROOT/etc/profile.d

%files
%{_prefix}/bin/lmp
%{_prefix}/share/man/man1/lmp*
%{_prefix}/share/lammps/*
%{_prefix}/etc/profile.d/*

%changelog
* Tue Mar 26 2024 majian <majian@kylinos.cn> - 2023.08.02-3
- Init package.